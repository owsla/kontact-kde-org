# Source code for website: kontact.kde.org

[Link to the website](https://kontact.kde.org)


## Installation instructions

```bash
gem install bundler --user-install
bundle install --path vendor/bundle
bundle exec jekyll serve
```

## License

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

A copy of the GNU Affero General Public License should be available at [kontact.kde.org/LICENSE/AGPL-3.0-or-later.txt](https://kontact.kde.org/LICENSES/AGPL-3.0-or-later.txt).

The content of the website (text and image) is also available under the Creative Commons Attribution Share Alike 4.0 International license. A copy of the license is availabe at [kontact.kde.org/LICENSES/CC-BY-SA-4.0.txt](https://kontact.kde.org/LICENSES/CC-BY-SA-4.0.txt).

For more information, see [KDE Licensing policy](https://community.kde.org/Policies/Licensing_Policy).
